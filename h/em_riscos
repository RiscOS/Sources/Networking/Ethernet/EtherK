/* Copyright 2002 Tematic Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    em_riscos.h                                       */
/*                                                            */
/* Purpose: RISC OS specific interface routines to the BSD    */
/*          driver code for the EtherK device driver.         */
/*                                                            */
/* Author:  J.R.Byrne. Some routines (mainly filtering) were  */
/*          adapted from Ether1 and EtherI.                   */
/**************************************************************/

#ifndef EtherK_em_riscos_h
#define EtherK_em_riscos_h

#include <stdint.h>
#include <stdbool.h>

#include <kernel.h>

#include <sys/types.h>

/* Nastiness. We want the definitions in sys/malloc.h, but not the  */
/* alternative definitions of malloc() and free(). We should see if */
/* we can tidy this up some day if possible.                        */
#undef KERNEL
#include <sys/malloc.h>
#define KERNEL

#include <sys/mbuf.h>
#include <sys/dcistructs.h>

/* Types */

typedef bool boolean_t;

struct adapter; /* Forward declaration - defined in if_em.h */
struct device;  /* Forward declaration - defined in em_device.h */
typedef struct device *device_t;

#include "if_em_hw.h"
#include "filter.h"

/* NVRAM settings */
typedef union
{
   uint32_t value;
   struct
   {
      uint32_t link   : 3;
      uint32_t mdix   : 2;
      uint32_t fc     : 2;
      uint32_t unused : 1;
   } fields;
} NVRAMData;

typedef enum
{
    link_auto = 0,
    link_1000,
    link_100_half,
    link_100_full,
    link_10_half,
    link_10_full,
    link_max
} ConfigLink;

/* Structure for keeping message log */

/* Size of buffer must be at least 255 + 1 + 5 + 1 */
#define MSGLOG_BUFSIZE 640

typedef struct
{
    unsigned    messages_logged;/* Total number of messages logged */
    int         last_msg_end;   /* Offset to byte after final byte of last entry written */
    int         top_msg_start;  /* Offset to first byte of last message in buffer */
    uint8_t     buffer[MSGLOG_BUFSIZE];
} MessageLog;

/* Macros */

#define device_set_desc_copy(a, b)

#define contigmalloc(size, type, flags, low, high, alignment, boundary) \
    em_ro_pci_memalloc(size, alignment, boundary)

#define contigfree(address, size, type) \
    em_ro_pci_memfree(address)

#define msec_delay(x) usec_delay(1000*(x))

/*#define splimp(a) em_ro_disable_dev_irq(adapter->dev)*/
/*#define splx(a) {if ((a) != 0) em_ro_enable_dev_irq(adapter->dev);}*/
#include <AsmUtils/irqs.h>
#define splimp(a) ensure_irqs_off()
#define splx(a) restore_irqs(a)

#define MAX(a,b) (((a)>(b))?(a):(b))

/* Function prototypes for if_em.c */

int  em_probe (device_t dev);
int  em_attach (device_t dev);
int  em_detach (device_t dev);
void em_stop (struct adapter * adapter);
int  em_intr (struct adapter * adapter);
void em_local_timer (struct adapter * adapter);
void em_update_stats_counters (struct adapter * adapter);
void em_clean_transmit_interrupts (struct adapter * adapter);
void em_print_link_status (struct adapter * adapter);
int  em_init (struct adapter * adapter);
int  em_setup_transmit_structures (struct adapter *);
int  em_setup_receive_structures (struct adapter *);
void em_free_transmit_structures (struct adapter *);
void em_free_receive_structures (struct adapter *);

/* Function prototypes for test.c */

void test_run (device_t dev);

/* Function prototypes for em_asm.s */

void em_asm_irq_trampoline (void);
void em_asm_callproto (DibRef dib, struct mbuf * frames, void (*fs_handler)(), uint32_t fs_pwptr);

/* Function prototypes for em_riscos.c */

void usec_delay(uint32_t delay);
void pci_write_config(device_t dev, int reg, uint32_t val, int width);
uint32_t pci_read_config(device_t dev, int reg, int width);
uint32_t vtophys (vm_offset_t v);
int  em_ro_start (device_t dev);
void em_ro_restart (device_t dev);
int  em_ro_irq_handler(_kernel_swi_regs *r, void *pw);
void em_ro_flush_wb (void);
void em_ro_init_supported_stats (struct stats * supported);
void em_ro_init_stats (device_t dev);
void em_ro_update_stats (device_t dev);
_kernel_oserror * em_ro_change_link (device_t dev, ConfigLink link, uint8_t mdix);
_kernel_oserror * em_ro_set_advertise(device_t dev, uint8_t setting);
_kernel_oserror * em_ro_set_flow(device_t dev, em_fc_type setting);
void em_ro_read_configuration (device_t dev);
void em_ro_check_options (device_t dev);
void em_ro_update_addrlevel (device_t dev);
void * em_ro_pci_memalloc (size_t size, uint32_t alignment, uint32_t boundary);
void em_ro_pci_memfree (void * address);
int em_ro_disable_dev_irq (device_t dev);
void em_ro_enable_dev_irq (device_t dev);
bool em_ro_get_pci_hw_address (device_t dev, uint8_t ** result);
bool em_ro_get_pci_io_address (device_t dev, uint32_t address_index, uint32_t * result);
bool em_ro_setup_intr (device_t dev);
void em_ro_teardown_intr(device_t dev);
void em_ro_filter_packet(struct adapter * adapter, uint8_t * packet, uint32_t len,
    struct mbuf ** m_head_ptr, FilterSingleRef * current_filter);
int em_ro_transmit (device_t dev, struct mbuf * m_head, int num_packets, uint8_t * edst, uint8_t * esrc, uint16_t type);
int af_reject(uint32_t level, uint8_t *pack_hdr, uint8_t *mac_addr);
void em_ro_watchdog(device_t dev, uint32_t now);
#ifdef DBG_STATS
void em_ro_init_dbg_stats (void);
uint32_t em_ro_get_time (void);
#endif

#endif
